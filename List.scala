// our own list
sealed abstract class Listt[A] {
  def fold(i : A)(f : (A,A) => A) : A =
    this match {
      case Cons(h,t) => t.fold(f(i,h))(f)
      case NNil() => i
    }

  def ++(rhs : Listt[A]) : Listt[A] = {
    this match {
      case Cons(h,t) => Cons(h,t ++ rhs )
      case NNil() => rhs
    }
  }

  def addToBack(a : A) : Listt[A] =
    this match {
      case Cons(h,t) => Cons(h,t.addToBack(a))
      case NNil() => Cons(a,NNil())
    }

  def reverse : Listt[A] = {
    this match {
      case Cons(h,t) => (t.reverse).addToBack(h)
      case NNil() => NNil()
    }
  }
  def reverseTail : Listt[A] = {
    def reverseAccum(l : Listt[A] , accum : Listt[A]) : Listt[A] = {
      l match {
        case Cons(h,t) => reverseAccum(t, Cons(h,accum))
        case NNil() => accum
      }
    }
    reverseAccum(this, NNil())
  }

  def map[B](f : A => B) : Listt[A] = {
    this match {
      case Cons(h,t) => Cons(f(h), t.map(f))
      case NNil() => NNil()
    }
  }

  def mapTail[B](f : A => B) : Listt[B] = {
    def mapAccum(l : Listt[A], accum : Listt[B]) : Listt[B] = {
      l match {
        case Cons(h,t) => mapAccum(t, Cons(f(h),accum))
        case NNil() => accum
      }
    }
    mapAccum(this,NNil()).reverse
  }







}
// written infix :: for list from standard library
case class Cons[A](head : A, tail : Listt[A]) extends Listt[A]
// called Nil in standard library
case class NNil[A]() extends Listt[A]