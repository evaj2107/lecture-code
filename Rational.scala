class Rational(initN : Int, initD : Int) {
  private val commonFactor = DiscreteMath.gcd(initN, initD)
  val n : Int = initN / commonFactor
  val d : Int = initD / commonFactor

  def this(initN : Int) = this(initN,1)

  override def toString : String =
    if(d == 1) n.toString
    else n.toString + "/" + d.toString

  def add(rhs : Rational) : Rational =
    new Rational(n * rhs.d + rhs.n * d,d * rhs.d)

  def +(rhs : Rational) : Rational = add(rhs)

  def ==(rhs : Rational) : Boolean = n == rhs.n && d == rhs.d

  def *(rhs : Rational) : Rational =
    new Rational(n * rhs.n, d * rhs.d)
}

object DiscreteMath {
  @scala.annotation.tailrec
  def gcd(a : Int, b : Int) : Int =
    if(b == 0) a else gcd(b,a % b)
}

